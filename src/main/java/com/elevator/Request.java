package com.elevator;

import java.time.Instant;

import static com.elevator.ElevatorController.*;

/**
 * Created by madalasa on 3/13/17.
 */
public abstract class Request {

    private final int floor;

    private final DIRECTION direction;

    private final Instant instant;


    public Request(int floor, DIRECTION direction, Instant instant) {
        this.floor = floor;
        this.direction = direction;
        this.instant = instant;
    }

    public int getFloor() {
        return floor;
    }

    public DIRECTION getDirection() {
        return direction;
    }

    public Instant getInstant() {
        return instant;
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof Request) &&
                ((Request)obj).getDirection() == this.getDirection() &&
                ((Request)obj).getFloor() == this.getFloor();
    }

    @Override
    public int hashCode() {
        return floor + 31*direction.hashCode();
    }

    @Override
    public String toString() {
        return new StringBuilder().append("{").append("floor:").append(floor)
                .append(", direction:").append(direction)
                .append(", instant:").append(instant)
                .append("}").toString();
    }
}
