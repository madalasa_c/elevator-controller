package com.elevator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Instant;

import static com.elevator.ElevatorController.*;
import static com.elevator.ElevatorController.DIRECTION.DOWN;
import static com.elevator.ElevatorController.DIRECTION.UP;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

/**
 * Created by madalasa on 3/13/17.
 */
class ElevatorTest {

    Elevator elevator;

    @BeforeEach
    public void setUp(){
        elevator = new Elevator(10);
    }

    @Test
    void addRequest() {
        Request request = new ElevatorRequest(5, UP, Instant.now());
        elevator.addRequest(request);

        assertEquals(request, elevator.removeRequest());
    }

    @Test
    void addMultipleRequest_up() {
        Request request1 = new ElevatorRequest(9, UP, Instant.now());
        Request request2 = new ElevatorRequest(6, UP, Instant.now());
        elevator.addRequest(request1);
        elevator.addRequest(request2);
        assertEquals(request2, elevator.removeRequest());
        assertEquals(request1, elevator.removeRequest());
    }


    @Test
    void addMultipleRequest_dups_up() {
        Request request1 = new ElevatorRequest(9, UP, Instant.now());
        Request request2 = new ElevatorRequest(6, UP, Instant.now());
        Request request3 = new ElevatorRequest(9, UP, Instant.now());
        elevator.addRequest(request1);
        elevator.addRequest(request2);
        elevator.addRequest(request3);
        assertEquals(request2, elevator.removeRequest());
        assertEquals(request1, elevator.removeRequest());
        assertNull(elevator.removeRequest());
    }

    @Test
    void addMultipleRequest_down() {
        Request request1 = new ElevatorRequest(5, DOWN, Instant.now());
        Request request2 = new ElevatorRequest(6, DOWN, Instant.now());
        elevator.addRequest(request1);
        elevator.addRequest(request2);
        assertEquals(request2, elevator.removeRequest());
        assertEquals(request1, elevator.removeRequest());
    }

    @Test
    void addMultipleRequest() {
        Request request1 = new ElevatorRequest(9, UP, Instant.now());
        Request request2 = new ElevatorRequest(5, DOWN, Instant.now());
        Request request3 = new ElevatorRequest(6, UP, Instant.now());
        elevator.addRequest(request1);
        elevator.addRequest(request2);
        elevator.addRequest(request3);
        assertEquals(request3, elevator.removeRequest());
        assertEquals(request1, elevator.removeRequest());
        assertEquals(request2, elevator.removeRequest());
    }

    @Test
    void move() throws InterruptedException {
        elevator.addRequest(new ElevatorRequest(8, UP, Instant.now()));
        elevator.move();
        while (elevator.getCurrentStatus() != STATUS.DOOR_OPEN) {
        }
        assertEquals(8, elevator.getCurrentFloor());
    }

    @Test
    void move_down_up() throws InterruptedException {
        elevator.addRequest(new ElevatorRequest(7, DOWN, Instant.now()));
        elevator.move();
        elevator.addRequest(new ElevatorRequest(3, UP, Instant.now()));

        waitTillDoorOpens();
        assertEquals(7, elevator.getCurrentFloor());
        elevator.move();

        waitTillDoorOpens();
        assertEquals(3, elevator.getCurrentFloor());
    }

    @Test
    void move_multiple_up_higher() throws InterruptedException{
        elevator.addRequest(new ElevatorRequest(7, DOWN, Instant.now()));
        elevator.addRequest(new ElevatorRequest(9, UP, Instant.now()));
        elevator.move();
        waitTillDoorOpens();
        assertEquals(7, elevator.getCurrentFloor());
        elevator.move();
        waitTillDoorOpens();
        assertEquals(9, elevator.getCurrentFloor());
    }


    @Test
    void move_up_down() throws InterruptedException{
        elevator.addRequest(new ElevatorRequest(8, DOWN, Instant.now()));
        elevator.move();
        waitTillDoorOpens();
        assertEquals(8, elevator.getCurrentFloor());
        elevator.addRequest(new ElevatorRequest(5, UP, Instant.now()));
        elevator.addRequest(new FloorRequest(6, DOWN, Instant.now()));
        elevator.move();
        waitTillDoorOpens();
        assertEquals(6, elevator.getCurrentFloor());
        elevator.move();
        waitTillDoorOpens();
        assertEquals(5, elevator.getCurrentFloor());
    }

    @Test
    void move_multiple() throws InterruptedException{
        elevator.addRequest(new ElevatorRequest(4, UP, Instant.now()));
        elevator.addRequest(new ElevatorRequest(2, UP, Instant.now()));
        elevator.move();
        waitTillDoorOpens();

        assertEquals(2, elevator.getCurrentFloor());
        elevator.addRequest(new ElevatorRequest(3, DOWN, Instant.now()));
        elevator.move();
        waitTillDoorOpens();

        assertEquals(4, elevator.getCurrentFloor());
        elevator.addRequest(new FloorRequest(6, UP, Instant.now()));
        elevator.move();

        waitTillDoorOpens();
        assertEquals(6, elevator.getCurrentFloor());
        elevator.move();

        waitTillDoorOpens();
        assertEquals(3, elevator.getCurrentFloor());
    }

    @Test
    void getCurrentFloor() {
        assertEquals(1, elevator.getCurrentFloor());
    }


    private void waitTillDoorOpens() {
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        while(elevator.getCurrentStatus() != STATUS.DOOR_OPEN){
        }
    }

}