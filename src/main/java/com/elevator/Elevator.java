package com.elevator;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static com.elevator.ElevatorController.DIRECTION;
import static com.elevator.ElevatorController.STATUS;

/**
 * Created by madalasa on 3/13/17.
 */
public class Elevator {

    private final Lock requestLock = new ReentrantLock();
    private final Lock statusLock = new ReentrantLock();
    private final Condition moveCondition = statusLock.newCondition();

    private final int totalFloors;
    private AtomicInteger currentFloor = new AtomicInteger(1);
    private STATUS currentStatus = STATUS.HOLD;
    private DIRECTION requestDirection;
    private ExecutorService executorService = Executors.newFixedThreadPool(1);

    //Assumption that the requests are processed based on time sequentially
    private Queue<Request> requestQueue = new PriorityQueue<>((request1, request2) -> {
        if(request1.getFloor() == request2.getFloor() && request1.getDirection() == request2.getDirection()){
            return 0;
        }

        if(requestDirection != null){
            if(request1.getDirection() == request2.getDirection()) {

                if(getNextProbableStatus() == STATUS.MOVING_UP){
                    Request higherPriority = request1.getFloor() < request2.getFloor() ? request1: request2;
                    if(higherPriority.getFloor() < currentFloor.get()){
                        return 1;
                    }
                    else
                        return -1;
                }
                else if(getNextProbableStatus() == STATUS.MOVING_DOWN){
                    Request higherPriority = request1.getFloor() > request2.getFloor() ? request1: request2;
                    if(higherPriority.getFloor() > currentFloor.get()){
                        return 1;
                    }
                    else
                        return -1;
                }
                else{
                    return request1.getFloor() < request2.getFloor() ? -1 * requestDirection.getFactor() : 1 * requestDirection.getFactor();
                }
            }
            else if(request1.getDirection() == requestDirection && request2.getDirection() != requestDirection){
                return -1;
            }
            else if(request2.getDirection() == requestDirection && request1.getDirection() != requestDirection){
                return 1;
            }
        }

        return request1.getInstant().isBefore(request2.getInstant())?-1:1;
    });

    public Elevator(int totalFloors) {
        this.totalFloors = totalFloors;
        executorService.execute(() -> start());
    }

    public void addRequest(Request request){
        try {
            requestLock.lock();
            requestQueue.add(request);
            if (requestDirection == null) {
                requestDirection = request.getDirection();
            }
        }finally {
            requestLock.unlock();
        }
    }

    public void move(){
        statusLock.lock();
        moveCondition.signal();
        statusLock.unlock();
    }

    public int getCurrentFloor() {
        return currentFloor.get();
    }


    public STATUS getCurrentStatus() {
        try {
            statusLock.lock();
            return currentStatus;
        }finally {
            statusLock.unlock();
        }
    }

    Request removeRequest(){
        try {
            requestLock.lock();
            Request nextRequest = requestQueue.poll();
            if (nextRequest != null) {
                //remove duplicates
                removeDuplicates(nextRequest);
            }
            return nextRequest;
        }
        finally {
            requestLock.unlock();
        }
    }

    private void setCurrentFloor(int floor){
        if(floor == totalFloors-1){
            setCurrentStatus(STATUS.MOVING_DOWN);
            currentFloor.set(totalFloors-1);
        }
        else if(floor == 1){
            setCurrentStatus(STATUS.MOVING_UP);
            currentFloor.set(1);
        }
        else
            this.currentFloor.set(floor);
    }

    private void removeDuplicates(Request nextRequest) {
        while (nextRequest.equals(requestQueue.peek())) {
            requestQueue.poll();
        }
    }

    private Request peekRequest(){
        try {
            requestLock.lock();
            return requestQueue.peek();
        }finally {
            requestLock.unlock();
        }
    }

    private void start() {
        statusLock.lock();
        try {
            while (true) {
                Request request;
                waitForRequest();

                while ((request = peekRequest()) != null) {
                    updateStatus(request);

                    setCurrentFloor((currentStatus == STATUS.MOVING_UP) ? currentFloor.addAndGet(1) : currentFloor.addAndGet(- 1));
                    if (getCurrentFloor() == request.getFloor()) {
                        setCurrentStatus(STATUS.DOOR_OPEN);
                        requestDirection = removeRequest().getDirection();
                        waitForMove();
                    }else{
                        waitForNextFloor();
                    }
                }
                setCurrentStatus(STATUS.HOLD);
                setCurrentFloor(1);
            }
        }finally {
            statusLock.unlock();
        }
    }

    private void waitForMove() {
        try {
            System.out.println("Thread " + Thread.currentThread() + ", Going to wait with currentStatus:" + getCurrentStatus() + ", currentFloor:" + currentFloor.intValue());
            moveCondition.await();
        } catch (InterruptedException e) {
        }
    }

    private void waitForNextFloor() {
        try {
            moveCondition.await(60, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
        }
    }

    private void waitForRequest() {
        while (peekRequest() == null) {
            waitForMove();
        }
    }

    private void updateStatus(Request request) {
        try {
            statusLock.lock();
            if (currentStatus == STATUS.HOLD || currentStatus == STATUS.DOOR_OPEN) {
                currentStatus = getNextStatus(request);
            }
        }
        finally {
            statusLock.unlock();
        }
    }

    private STATUS getNextProbableStatus() {
        try {
            statusLock.lock();
            if (currentStatus == STATUS.DOOR_OPEN) {
                return getNextStatus(peekRequest());
            }
            return currentStatus;
        }
        finally {
            statusLock.unlock();
        }
    }

    private STATUS getNextStatus(Request request) {
        return (currentFloor.get() <= request.getFloor()) ? STATUS.MOVING_UP : STATUS.MOVING_DOWN;
    }

    private void setCurrentStatus(STATUS currentStatus) {
        try {
            statusLock.lock();
            this.currentStatus = currentStatus;
        }finally {
            statusLock.unlock();
        }
    }

    public void stop() throws InterruptedException {
        executorService.shutdownNow();
        executorService.awaitTermination(1000, TimeUnit.MILLISECONDS);
    }
}
