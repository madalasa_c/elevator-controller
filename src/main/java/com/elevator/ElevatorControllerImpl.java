package com.elevator;

import java.time.Instant;

/**
 * Created by madalasa on 3/13/17.
 */
public class ElevatorControllerImpl implements ElevatorController {

    private static final int TOTAL_FLOORS = 10;

    private final Elevator elevator = new Elevator(TOTAL_FLOORS);

    @Override
    public int currentFloor() {
        return elevator.getCurrentFloor();
    }

    @Override
    public void requestElevator(int floor, DIRECTION direction) {
        elevator.addRequest(new ElevatorRequest(floor, direction, Instant.now()));
        if(elevator.getCurrentStatus() == STATUS.HOLD)
            elevator.move();
    }

    @Override
    public void selectFloor(int floor) {
        elevator.addRequest(new FloorRequest(floor, (elevator.getCurrentFloor() < floor)? DIRECTION.UP: DIRECTION.DOWN, Instant.now()));
    }

    @Override
    public void moveElevator() {
        if(elevator.getCurrentStatus() == STATUS.HOLD || elevator.getCurrentStatus() == STATUS.DOOR_OPEN)
            elevator.move();
    }

    @Override
    public STATUS getElevatorStatus() {
        return elevator.getCurrentStatus();
    }

    @Override
    public void tearDown() {
        try {
            elevator.stop();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
