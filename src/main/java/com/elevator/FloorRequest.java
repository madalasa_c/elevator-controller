package com.elevator;

import java.time.Instant;

import static com.elevator.ElevatorController.DIRECTION;

/**
 * Created by madalasa on 3/13/17.
 */
public class FloorRequest extends Request {

    public FloorRequest(int level, DIRECTION direction, Instant instant) {
        super(level, direction, instant);
    }

    @Override
    public String toString() {
        return new StringBuilder().append("FloorRequest:").append(super.toString()).toString();
    }
}
