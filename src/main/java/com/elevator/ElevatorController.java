package com.elevator;

/**
 * Created by madalasa on 3/13/17.
 */
public interface ElevatorController {

    enum DIRECTION{
        UP(1), DOWN(-1);

        private int factor;

        DIRECTION(int factor) {
            this.factor = factor;
        }

        public int getFactor() {
            return factor;
        }
    }


    enum STATUS{
        MOVING_UP, MOVING_DOWN, DOOR_OPEN, HOLD
    }

    int currentFloor();

    void requestElevator(int floor, DIRECTION direction);

    void selectFloor(int floor);

    void moveElevator();

    STATUS getElevatorStatus();

    void tearDown();
}
