package com.elevator;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.elevator.ElevatorController.DIRECTION.DOWN;
import static com.elevator.ElevatorController.DIRECTION.UP;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by madalasa on 3/13/17.
 */
class ElevatorControllerTest {

    ElevatorController elevatorController;

    @BeforeEach
    void setUp(){
        elevatorController = new ElevatorControllerImpl();
    }

    @AfterEach
    void tearDown(){
        elevatorController.tearDown();
    }

    @Test
    void currentFloor() {
        assertEquals(1, elevatorController.currentFloor());
    }

    @Test
    void requestElevator_Up() throws InterruptedException {
        int currentLevel = 5;
        elevatorController.requestElevator(currentLevel, UP);
        waitTillDoorOpens();
        assertEquals(currentLevel, elevatorController.currentFloor());
    }


    @Test
    void requestElevator_Down() throws InterruptedException {
        int currentLevel = 5;
        elevatorController.requestElevator(currentLevel, DOWN);
        waitTillDoorOpens();
        assertEquals(currentLevel, elevatorController.currentFloor());
    }

    @Test
    void selectFloor_Up() throws InterruptedException {
        int currentLevel = 6;
        elevatorController.requestElevator(currentLevel, UP);
        waitTillDoorOpens();
        assertEquals(currentLevel, elevatorController.currentFloor());

        elevatorController.selectFloor(9);
        elevatorController.moveElevator();
        waitTillDoorOpens();
        assertEquals(9, elevatorController.currentFloor());
    }


    @Test
    void selectFloor_Down() throws InterruptedException {
        int currentLevel = 8;
        elevatorController.requestElevator(currentLevel, DOWN);
        waitTillDoorOpens();
        assertEquals(currentLevel, elevatorController.currentFloor());

        elevatorController.selectFloor(4);
        elevatorController.moveElevator();
        waitTillDoorOpens();
        assertEquals(4, elevatorController.currentFloor());
    }

    @Test
    void moveElevator() throws InterruptedException{
        elevatorController.requestElevator(8, DOWN);
        waitTillDoorOpens();
        assertEquals(8, elevatorController.currentFloor());

        elevatorController.selectFloor(5);
        elevatorController.moveElevator();
        waitTillDoorOpens();
        assertEquals(5, elevatorController.currentFloor());
    }


    @Test
    void requestElevator_multiple_up() throws InterruptedException {
        elevatorController.requestElevator(8, UP);
        elevatorController.requestElevator(4, UP);

        waitTillDoorOpens();
        assertEquals(4, elevatorController.currentFloor());
        elevatorController.moveElevator();
        waitTillDoorOpens();
        assertEquals(8, elevatorController.currentFloor());
    }


    @Test
    void requestElevator_up_down() throws InterruptedException {
        elevatorController.requestElevator(8, UP);
        elevatorController.requestElevator(4, DOWN);

        waitTillDoorOpens();
        assertEquals(8, elevatorController.currentFloor());
        elevatorController.moveElevator();

        waitTillDoorOpens();
        assertEquals(4, elevatorController.currentFloor());
    }

    @Test
    void requestElevator_multiple_up_down() throws InterruptedException {
        elevatorController.requestElevator(8, UP);
        elevatorController.requestElevator(4, UP);
        waitTillDoorOpens();
        assertEquals(4, elevatorController.currentFloor());

        elevatorController.requestElevator(3, UP);
        elevatorController.moveElevator();
        waitTillDoorOpens();

        assertEquals(8, elevatorController.currentFloor());
        elevatorController.moveElevator();
        waitTillDoorOpens();

        assertEquals(3, elevatorController.currentFloor());
    }

    @Test
    void requestAndSelectElevator_multiple_up() throws InterruptedException {
        elevatorController.requestElevator(9, UP);
        elevatorController.requestElevator(3, UP);
        waitTillDoorOpens();

        assertEquals(3, elevatorController.currentFloor());
        elevatorController.selectFloor(6);
        elevatorController.moveElevator();

        waitTillDoorOpens();
        assertEquals(6, elevatorController.currentFloor());
        elevatorController.moveElevator();

        waitTillDoorOpens();
        assertEquals(9, elevatorController.currentFloor());
    }


    @Test
    void requestAndSelectElevator_multiple_up_down() throws InterruptedException {
        elevatorController.requestElevator(4, UP);
        elevatorController.requestElevator(8, DOWN);

        waitTillDoorOpens();
        assertEquals(4, elevatorController.currentFloor());

        elevatorController.selectFloor(9);
        elevatorController.moveElevator();
        waitTillDoorOpens();

        assertEquals(9, elevatorController.currentFloor());

        elevatorController.moveElevator();
        waitTillDoorOpens();
        assertEquals(8, elevatorController.currentFloor());
    }

    private void waitTillDoorOpens() {
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        while(elevatorController.getElevatorStatus() != ElevatorController.STATUS.DOOR_OPEN){
        }
    }

}